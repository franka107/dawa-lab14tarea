const express = require("express");
const http = require("http");
const path = require("path");
const socketIo = require("socket.io");
const { Usuarios } = require("./classes/usuario");
const { crearMensaje } = require("./utilidades/utilidades");
//webpack
const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const config = require("./webpack.config");

const app = express();
const server = http.createServer(app);
const io = socketIo(server);
const usuarios = new Usuarios();

//midlleware
app.use(webpackDevMiddleware(webpack(config)));
app.use(express.static(path.join(__dirname, "public")));

io.on("connection", (client) => {
  console.log("socket connected", client.id);
  client.on("crearMensaje", (data) => {
    let persona = usuarios.getPersona(client.id);
    let mensaje = crearMensaje(persona.nombre, data.mensaje);
    client.broadcast.emit("crearMensaje", mensaje);
  });

  client.on("mensajePrivado", (data) => {
    let persona = usuarios.getPersona(client.id);

    client.broadcast
      .to(data.para)
      .emit("mensajePrivado", crearMensaje(persona.nombre, data.mensaje));
  });

  client.on("entrarChat", (data, callback) => {
    if (!data.nombre || !data.sala) {
      return callback({
        error: true,
        mensaje: "el nombre es necesario",
      });
    }

    client.join(data.sala);
    usuarios.agregarPersona(client.id, data.nombre, data.sala);

    client.broadcast
      .to(data.sala)
      .emit("listarPersonas", usuarios.getPersonasPorSala(data.sala));

    callback(usuarios.getPersonasPorSala(data.sala));
  });
});

server.listen(3000, () => {
  console.log("servidor iniciado en el puerto 3000");
});
