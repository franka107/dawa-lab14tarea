import React from "react";
import { render } from "react-dom";
import io from "socket.io-client";

class App extends React.Component {
  componentDidMount() {
    this.socket = io();
    this.socket.on("listarPersonas", (usuarios) => {
      console.log("estos son los usuarios en la sala");
      console.log(usuarios);
      this.setState({ personasenlasala: usuarios });
    });
    this.socket.on("crearMensaje", (mensaje) => {
      this.setState({ messages: [...this.state.messages, mensaje] });
    });
  }
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      user: "",
      chat: "",
      loguedIn: false,
      personasenlasala: [],
      mymessage: "",
    };
    this.handleUser = this.handleUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChat = this.handleChat.bind(this);
    this.handleMymessage = this.handleMymessage.bind(this);
    this.submitMessage = this.submitMessage.bind(this);
  }

  handleMymessage(e) {
    this.setState({ mymessage: e.target.value });
  }

  submitMessage() {
    this.socket.emit(
      "crearMensaje",
      {
        nombre: this.state.user,
        mensaje: this.state.mymessage,
      },
      function (mensaje) {
        console.log("guardando mensaje en array");
        console.log(this.state.messages);
      }
    );
    this.setState({
      messages: [
        ...this.state.messages,
        {
          nombre: this.state.user,
          mensaje: this.state.mymessage,
          fecha: new Date().getTime(),
        },
      ],
    });
    this.setState({ mymessage: "" });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.socket.emit(
      "entrarChat",
      {
        nombre: this.state.user,
        sala: this.state.chat,
      },
      (personasenlasala) => {
        console.log("todo okey");
        this.setState({ loggedIn: true });
        this.setState({ personasenlasala: personasenlasala });
        console.log(personasenlasala);
      }
    );
  }

  handleUser(e) {
    this.setState({ user: e.target.value });
  }
  handleChat(e) {
    this.setState({ chat: e.target.value });
  }

  render() {
    const User = function ({ id, nombre, sala }) {
      return (
        <li>
          <a href="javascript:void(0)">
            <img
              src="https://www.unasam.edu.pe/investigacion/files/images/usuario-masculino.jpg"
              alt="user-img"
              class="img-circle"
            />{" "}
            <span>
              {nombre} <small class="text-success">online</small>
            </span>
          </a>
        </li>
      );
    };

    const Mensaje = function ({ mensaje, yo }) {
      let fecha = new Date(mensaje.fecha);
      let hora = fecha.getHours() + ":" + fecha.getMinutes();
      if (yo) {
        return (
          <li class="reverse">
            <div class="chat-content">
              <h5>{mensaje.nombre}</h5>
              <div class="box bg-light-inverse">{mensaje.mensaje}</div>
            </div>
            <div class="chat-img">
              <img src="assets/images/users/5.jpg" alt="user" />
            </div>
            <div class="chat-time">{hora}</div>
          </li>
        );
      } else {
        return (
          <li>
            <div class="chat-img">
              <img src="assets/images/users/2.jpg" alt="user" />
            </div>
            <div class="chat-content">
              <h5>{mensaje.nombre}</h5>
              <div class="box bg-light-info">{mensaje.mensaje}</div>
            </div>
            <div class="chat-time">{hora}</div>
          </li>
        );
      }
    };
    if (!this.state.loggedIn) {
      return (
        <React.Fragment>
          <section id="wrapper">
            <div className="login-register">
              <div className="login-box card">
                <div className="card-body">
                  <form
                    className="form-horizontal form-material"
                    onSubmit={this.handleSubmit}
                  >
                    <h3 className="box-title m-b-20">Ingresar al Chat</h3>
                    <div className="form-group ">
                      <div className="col-xs-12">
                        <input
                          className="form-control"
                          value={this.state.user}
                          onChange={this.handleUser}
                          type="text"
                          placeholder="Nombre de usuario"
                          name="nombre"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-xs-12">
                        <input
                          className="form-control"
                          type="text"
                          required=""
                          placeholder="Sala de chat"
                          name="sala"
                          value={this.state.chat}
                          onChange={this.handleChat}
                        />
                      </div>
                    </div>

                    <div className="form-group text-center">
                      <div className="col-xs-12 p-b-20">
                        <button
                          className="btn btn-block btn-lg btn-info btn-rounded"
                          type="submit"
                        >
                          Ingresar al chat
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </section>
        </React.Fragment>
      );
    } else {
      return (
        <div class="fix-header single-column card-no-border fix-sidebar">
          <div id="main-wrapper">
            <div class="page-wrapper">
              <div class="container-fluid">
                <div class="row animated fadeIn">
                  <div class="col-12">
                    <div class="card m-b-0">
                      <div class="chat-main-box">
                        <div class="chat-left-aside">
                          <div class="open-panel">
                            <i class="ti-angle-right"></i>
                          </div>
                          <div class="chat-left-inner">
                            <div class="form-material">
                              <input
                                class="form-control p-20"
                                type="text"
                                placeholder="Buscar Contacto"
                              />
                            </div>
                            <ul class="chatonline style-none" id="divUsuarios">
                              <li>
                                <a href="javascript:void(0)" class="active">
                                  {" "}
                                  Chat de <span> Juegos</span>
                                </a>
                              </li>
                              {this.state.personasenlasala.map((persona) => {
                                return (
                                  <User
                                    id={persona.id}
                                    sala={persona.sala}
                                    nombre={persona.nombre}
                                  />
                                );
                              })}
                              <li class="p-20"></li>
                            </ul>
                          </div>
                        </div>
                        <div class="chat-right-aside">
                          <div class="chat-main-header">
                            <div class="p-20 b-b">
                              <h3 class="box-title">
                                Sala de chat <small>{this.state.chat}</small>
                              </h3>
                            </div>
                          </div>

                          <div class="chat-rbox">
                            <ul class="chat-list p-20" id="divChatbox">
                              {this.state.messages.map((message) => {
                                return (
                                  <Mensaje
                                    mensaje={message}
                                    yo={
                                      message.nombre === this.state.user
                                        ? true
                                        : false
                                    }
                                  />
                                );
                              })}
                            </ul>
                          </div>
                          <div class="card-body b-t">
                            <div class="row">
                              <div class="col-8">
                                <textarea
                                  placeholder="Escribe tu mensaje aquí"
                                  class="form-control b-0"
                                  value={this.state.mymessage}
                                  onChange={this.handleMymessage}
                                ></textarea>
                              </div>
                              <div class="col-4 text-right">
                                <button
                                  type="button"
                                  class="btn btn-info btn-circle btn-lg"
                                  onClick={this.submitMessage}
                                >
                                  <i class="fa fa-paper-plane-o"></i>{" "}
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <footer class="footer"></footer>
            </div>
          </div>
        </div>
      );
    }
  }
}

render(<App></App>, document.getElementById("app"));
